import React, { useState, useRef, useEffect } from "react";
import {
  FormLabel,
  FormControl,
  VStack,
  Flex,
  Spinner,
  Box,
  Button,
  Avatar,
  useDisclosure,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import forms from "src/forms/tickets";
import list from "src/data";
import _ from "underscore";
import { API, URL } from "src/config/api";

//redux
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "src/redux/user/actions";

//components
import { useToastHook } from "src/components/toast";
import Input from "src/components/input";
import ImageCropper from "src/components/image-cropper";
import Modal from "src/components/modal";
import { canvasToBlob } from "src/components/image-cropper/utils";

const Tickets = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const user = useSelector((state) => state.user.data);
  const inputRefs = useRef([]);
  let defaultValues = {};
  Object.entries(forms).map((item) => {
    const key = item[0];
    const inputType = item[1].type;
    let value = "";
    switch (inputType) {
      default:
        value = user[key];
        break;
    }
    defaultValues[key] = value;
  });
  const {
    handleSubmit,
    register,
    getValues,
    control,
    reset,
    formState: { isDirty, dirtyFields, errors, isSubmitting },
  } = useForm({ defaultValues });
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [uploadedImage, setUploadedImage] = useState(null);
  const [toast, makeToast] = useToastHook();
  const formIsInvalid = Object.keys(getValues()).some((key) => errors[key]);
  const filteredForms = _.omit(forms, "image", "cv");

  useEffect(() => {
    if (uploadedImage) {
      onOpen(true);
    }
  }, [uploadedImage]);

  const onClickUploadInput = () => {
    const image = getValues("image");
    if (image) {
      onOpen(true);
      setUploadedImage(image);
    } else {
      inputRefs.current["uploadImageInput"].click();
    }
  };

  const onChangeUploadInput = async () => {
    setUploadedImage(inputRefs.current["uploadImageInput"].files[0]);
  };

  const onCloseCropperModal = (image) => {
    reset({
      ...getValues(),
      image: image.url,
    });
    let formData = { ...getValues() };
    canvasToBlob(image.canvas, uploadedImage.name, uploadedImage.type).then(
      (blob) => {
        formData.image = blob;
        onSubmit(formData);
      }
    );
    onClose(true);
  };

  const onChangeUploadNewImage = () => {
    const formData = { ...getValues() };
    formData.image = "";
    reset(formData);
    onClose(true);
    onClickUploadInput();
  };

  const onSubmit = (obj) => {
    let formData = new FormData();
    Object.keys(obj).map((key) => {
      if (key === "image") {
        if (typeof obj[key] === "object") {
          formData.append("image", obj[key], obj[key].name);
        } else {
          formData.append("image", obj[key]);
        }
      } else {
        formData.append(key, obj[key]);
      }
    });
    dispatch(updateUser(formData, user._id))
      .then((res) => {
        const {
          data: { data, message },
        } = res;
        reset(data);
        makeToast({
          title: message,
          status: "success",
        });
      })
      .catch((err) => {
        let errorArray = [];
        if (Array.isArray(err)) {
          errorArray = err;
        } else {
          errorArray.push(err.message);
        }
        errorArray.map((error) => {
          setTimeout(() => {
            makeToast({
              title: error || "مشکلی در ارتباط با سرور وجود دارد",
              status: "error",
            });
          }, 50);
        });
      });
  };

  return (
    <VStack
      dir="rtl"
      alignItems={"center"}
      justify={"flex-start"}
      p="30px"
      w={"100%"}
      h={"100%"}
    >
      {loading ? (
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      ) : (
        <Flex justify={"center"} align={"center"} flexDir={"column"}>
          <input
            hidden
            id="xlxsFileInput"
            type="file"
            value={""}
            onChange={(e) => onChangeUploadInput(e)}
            ref={(el) => (inputRefs.current["uploadImageInput"] = el)}
          />
          <Avatar
            size="2xl"
            name=""
            src={getValues("image")}
            mt="30px"
            mb="50px"
            cursor={"pointer"}
            htmlFor="xlxsFileInput"
            onClick={onClickUploadInput}
          />
          <form onSubmit={handleSubmit(onSubmit)} className="custom-form">
            <FormControl isInvalid={formIsInvalid} w="100%">
              {Object.entries(filteredForms).map((item) => {
                const fieldName = item[0];
                const input = item[1];
                let { label, placeholder, validations, disableOnUpdate, type } =
                  input;
                validations.required = `پر کردن فیلد ${label} الزامی می باشد`;
                return (
                  <Box
                    key={`fieldName_${fieldName}`}
                    mb="20px"
                    w="50%"
                    float={"left"}
                    px="10px"
                  >
                    <FormLabel htmlFor={fieldName} color="black">
                      {label}
                    </FormLabel>
                    <Input
                      config={{
                        fieldName,
                        placeholder,
                        type,
                        control,
                        data: list[fieldName] || [],
                        value: getValues(fieldName),
                        hookFormConfig: { ...register(fieldName, validations) },
                        disableOnUpdate,
                        error: errors[fieldName],
                      }}
                    />
                  </Box>
                );
              })}
            </FormControl>
            <Box px="10px">
              <Button
                my={"30px"}
                type="submit"
                isLoading={isSubmitting}
                isDisabled={!isDirty}
              >
                ویرایش اطلاعات پروفایل
              </Button>
            </Box>
          </form>
        </Flex>
      )}
      <Modal isOpen={isOpen} onClose={onClose} title={"انتخاب عکس"} size="2xl">
        <ImageCropper
          closeModal={onCloseCropperModal}
          image={uploadedImage}
          uploadNewImage={onChangeUploadNewImage}
        />
      </Modal>
    </VStack>
  );
};

export default Tickets;

import styled from "styled-components";
import Icon from "src/config/icon";

export const MenuIcon = styled(Icon).attrs(() => ({
  size: 20,
}))`
  color: "white";
  /* &.vertical-align {
    margin-left: 10px;
    vertical-align: middle;
  } */
`;

import { useDispatch, useSelector } from "react-redux";

const DashboardTabs = ({ children, type }) => {
  const dashboard = useSelector(state => state.dashboard);
  const dispatch = useDispatch();

  return children;
};

export default DashboardTabs;

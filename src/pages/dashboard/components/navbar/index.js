import React from "react";
import { Image, Flex, WrapItem, Button } from "@chakra-ui/react";
import {
  useNavigate,
  useResolvedPath,
  useLocation,
  NavLink,
} from "react-router-dom";

//redux
import { useDispatch } from "react-redux";
import { logoutUser } from "src/redux/user/actions";

//components
import { MenuIcon, BtnIcon } from "./style";
import { IMAGES } from "src/images";
import config from "src/config";

const Navbar = () => {
  const origin = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  let resolved = useResolvedPath(origin);

  const onClickMenuItem = (item) => () => {
    navigate(`/dashboard/${item.slug}`, { replace: true });
  };

  return (
    <Flex width={"100%"} flexDir="column">
      <NavLink to="/">
        <Image
          src={IMAGES.logo}
          width={"50%"}
          py="30px"
          display={"block"}
          m={"0 auto"}
        />
      </NavLink>
      <Flex
        direction={"column"}
        align="center"
        borderWidth={"1px 0px 1px 0px"}
        borderStyle="solid"
        borderColor="#e2e8f0"
        py={"10px"}
        px={"25px"}
        mb={"35px"}
      >
        {Object.values(config.dashboardTabs.admin).map((nav, index) => {
          const activeLink = resolved.pathname === `/dashboard/${nav.slug}`;
          return (
            <Flex
              w={"100%"}
              align={"center"}
              key={`dashboard_menu_${index}`}
              onClick={onClickMenuItem(nav)}
              py={"20px"}
            >
              <MenuIcon
                icon={nav.icon}
                size={"26px"}
                active={activeLink === true ? "true" : "false"}
              />
              <WrapItem
                color={activeLink ? "#344cb1" : "black"}
                fontSize={"md"}
                textAlign={"center"}
                width={"100%"}
                borderRadius="2xl"
                cursor={"pointer"}
              >
                {nav.title}
              </WrapItem>
            </Flex>
          );
        })}
      </Flex>
      <Button
        variant="outline"
        display={"flex"}
        margin={"0 auto"}
        mb={"35px"}
        w="85%"
        onClick={() => dispatch(logoutUser())}
      >
        <BtnIcon icon={"logout"} size={"20px"} />
        خروج از حساب کاربری
      </Button>
    </Flex>
  );
};

export default Navbar;

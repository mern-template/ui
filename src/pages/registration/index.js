import React, { useEffect } from "react";
import { Container, HStack, Center } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { IMAGES } from "src/images";

//redux
import { useSelector } from "react-redux";

//components
import RegistrationForm from "src/components/forms/registration";

const LoginPage = () => {
  const navigate = useNavigate();
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (!user.isFetching && user.isAuthenticated) {
      navigate("/dashboard");
    }
  }, [user.isFetching]);

  return (
    <HStack spacing="0px">
      <Center
        w="100%"
        h="100vh"
        bgImage={`linear-gradient(to right, rgba(50, 0, 0,.75), rgba(0, 0, 0,.75)) , url(${IMAGES.login})`}
        py="100px"
        bgPosition="center"
        bgRepeat="no-repeat"
        bgSize={"auto"}
        bgAttachment="fixed"
        position={"relative"}
      >
        <Container
          display={"flex"}
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <RegistrationForm />
        </Container>
      </Center>
    </HStack>
  );
};

export default LoginPage;

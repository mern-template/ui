const pages = [
  {
    directory: "registration",
    url: "registration",
    privateRoute: false,
    index: true,
    children: [],
  },
  {
    directory: "dashboard",
    url: "dashboard",
    privateRoute: true,
    index: true,
    children: [
      {
        directory: "tickets",
        url: "tickets",
      },
    ],
  },
];

export default pages;

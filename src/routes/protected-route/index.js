import { Navigate, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ children }) => {
  const user = useSelector((state) => state.user);
  const location = useLocation();

  if (!user.isAuthenticated) {
    return <Navigate to="/registration" replace state={{ from: location }} />;
  }

  return children;
};

export default ProtectedRoute;

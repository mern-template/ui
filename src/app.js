import React, { useEffect } from "react";
import { Routes, Route, Navigate } from "react-router-dom";

//redux
import { useDispatch } from "react-redux";
import { setCurrentUser } from "src/redux/user/actions";

//pages
import pages from "./routes";
import ProtectedRoute from "src/routes/protected-route";

//components

import Loader from "src/components/loader";

//loadable
import loadable from "@loadable/component";
import pMinDelay from "p-min-delay";

// css
import "react-day-picker/dist/style.css";

const LoadablePage = loadable(
  ({ directory }) => {
    return pMinDelay(import(`src/pages/${directory}`), 0);
  },
  {
    cacheKey: (props) => props.directory,
  }
);
const getIndexRoute = pages.filter((route) => route.index === true);
export const indexRoute = getIndexRoute ? getIndexRoute[0] : pages[0];

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const token = localStorage.getItem("userToken");
    if (token) {
      dispatch(setCurrentUser(token));
    }
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Navigate to={indexRoute.url} />} />
      {pages.map(({ directory, url, children, privateRoute, index }) => {
        const pageHaveChild = children.length > 0;
        let parentRouteElement = (
          <LoadablePage fallback={<Loader />} directory={directory} />
        );
        if (privateRoute && privateRoute === true) {
          parentRouteElement = (
            <ProtectedRoute>
              <LoadablePage fallback={<Loader />} directory={directory} />
            </ProtectedRoute>
          );
        }
        if (!pageHaveChild) {
          return (
            <Route
              key={`route_${directory}_${index}`}
              path={url}
              element={parentRouteElement}
            />
          );
        } else {
          return (
            <Route
              key={`parent_route_${directory}_${index}`}
              element={parentRouteElement}
              path={url}
            >
              <Route index element={<Navigate to={children[0].url} />} />
              {children.map((child) => {
                const childDirectory = `${url}/pages/${child.directory}`;
                let childRouteElement = (
                  <LoadablePage
                    fallback={<Loader />}
                    directory={childDirectory}
                  />
                );
                if (privateRoute && privateRoute === true) {
                  childRouteElement = (
                    <ProtectedRoute>
                      <LoadablePage
                        fallback={<Loader />}
                        directory={childDirectory}
                      />
                    </ProtectedRoute>
                  );
                }
                return (
                  <Route
                    key={`child_route_${directory}_${index}`}
                    path={`/${url}/${child.url}`}
                    element={childRouteElement}
                  />
                );
              })}
              <Route path="*" element={<p>error directory</p>} />
            </Route>
          );
        }
      })}
      <Route path="*" element={<p>error directory</p>} />
    </Routes>
  );
};

export default App;

import validatoins from "src/forms/validations";

const baseForm = {
  email: {
    label: "ایمیل",
    placeholder: "ایمیل",
    type: "text",
    validations: {
      required: "پر کردن این فیلد الزامی میباشد",
      pattern: {
        value: validatoins.email,
        message: "فرمت ایمیل وارد شده معتبر نمی باشد",
      },
    },
  },
  password: {
    label: "کلمه عبور",
    placeholder: "کلمه عبور",
    type: "password",
    validations: {
      required: "پر کردن این فیلد الزامی میباشد",
      minLength: {
        value: 6,
        message: "وارد کردن حداقل 6 کاراکتر الزامی میباشد",
      },
    },
  },
};

const forms = {
  login: {
    ...baseForm,
  },
  register: {
    ...baseForm,
    rePassword: {
      label: "تکرار کلمه عبور",
      placeholder: "تکرار کلمه عبور",
      type: "password",
      validations: {
        required: "پر کردن این فیلد الزامی میباشد",
        minLength: {
          value: 6,
          message: "وارد کردن حداقل 6 کاراکتر الزامی میباشد",
        },
      },
    },
    sex: {
      label: "جنسیت",
      placeholder: "انتخاب جنسیت",
      type: "select",
      validations: {},
    },
  },
};

export default forms;

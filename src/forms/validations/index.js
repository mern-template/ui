const validatoins = {
  email: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,3}$/, //only 11 character with format(09124375365)
  phoneNumber: /^(0)?9\d{9}$/g, //only 11 character with format(09124375365)
  englishCharacter: /^[a-zA-Z0-9_]*$/, //only En character with format(ehsan-vaziri , ehsan_vaziri with numeric in all positions)
  persianCharacter: /^[\u0600-\u06FF\s]+$/, //only Fa character with space and without any special character (احسان وزیری)
  nationalCode: (code) => {
    if (code.length !== 10 || /(\d)(\1){9}/.test(code)) return false;
    let sum = 0,
      chars = code.split(""),
      lastDigit,
      remainder;
    for (let i = 0; i < 9; i++) sum += +chars[i] * (10 - i);
    remainder = sum % 11;
    lastDigit = remainder < 2 ? remainder : 11 - remainder;
    return +chars[9] === lastDigit;
  },
};

export default validatoins;

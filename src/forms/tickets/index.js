import validatoins from "src/forms/validations";

const forms = {
  email: {
    label: "ایمیل",
    placeholder: "ایمیل",
    type: "text",
    disableOnUpdate: true,
    validations: {
      pattern: {
        value: validatoins.email,
        message: "فرمت ایمیل وارد شده معتبر نمی باشد",
      },
    },
  },
  username: {
    label: "نام کاربری",
    placeholder: "نام کاربری",
    type: "text",
    validations: {
      minLength: {
        value: 6,
        message: "وارد کردن حداقل 6 کاراکتر الزامی میباشد",
      },
      pattern: {
        value: validatoins.englishCharacter,
        message:
          "فقط کاراکتر انگلیسی همراه با (-) (_) و بدون فاصله قابل قبول می باشد",
      },
    },
  },
  phoneNumber: {
    label: "شماره تلفن",
    placeholder: "مثلا 09124375365",
    type: "number",
    validations: {
      minLength: {
        value: 11,
        message: "شماره موبایل وارد شده باید 11 رقم باشد",
      },
      maxLength: {
        value: 11,
        message: "شماره موبایل وارد شده باید 11 رقم باشد",
      },
      pattern: {
        value: validatoins.phoneNumber,
        message: "فرمت استاندارد شماه موبایل 09121234567",
      },
    },
  },
  fullName: {
    label: "نام و نام‌خانوادگی",
    placeholder: "نام و نام‌خانوادگی",
    type: "text",
    validations: {
      minLength: {
        value: 4,
        message: "وارد کردن حداقل 4 کاراکتر الزامی میباشد",
      },
      pattern: {
        value: validatoins.persianCharacter,
        message: "فقط کاراکتر فارسی قابل قبول می باشد",
      },
    },
  },
  birthDate: {
    label: "تاریخ تولد",
    placeholder: "انتخاب تاریخ تولد",
    type: "date",
    validations: {},
  },
  province: {
    label: "شهر",
    placeholder: "انتخاب شهر",
    type: "select",
    validations: {},
  },
  id: {
    label: "کد ملی",
    placeholder: "کد ملی",
    type: "text",
    validations: {
      validate: {
        validateNationalCode: (code) =>
          validatoins.nationalCode(code) || "کد ملی وارد شده معتبر نمی باشد",
      },
    },
  },
  education: {
    label: "تحصیلات",
    placeholder: "انتخاب میزان تحصیلات",
    type: "select",
    validations: {},
  },
  field: {
    label: "رشته",
    placeholder: "رشته",
    type: "text",
    validations: {},
  },
  job: {
    label: "شفل",
    placeholder: "شفل",
    type: "text",
    validations: {},
  },
  sex: {
    label: "جنسیت",
    placeholder: "انتخاب جنسیت",
    type: "select",
    validations: {},
  },
  image: {
    label: "آپلود عکس پروفایل",
    placeholder: "آپلود عکس پروفایل",
    type: "imageUpload",
    validations: {},
  },
  // cv: {
  //   label: "فایل رزومه",
  //   placeholder: "فایل رزومه",
  //   type: "fileUpload",
  //   validations: {},
  // },
};

export default forms;

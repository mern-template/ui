export const tablesObject = {};

export const tablesArray = (key) => {
  let array;
  return (array = Object.entries(tablesObject[key]).map((item) => {
    return { accessor: item[0], Header: item[1] };
  }));
};

import axios from "axios";
import config from "src/config";
import url from "src/config/api/url";

export const URL = url;
export const API = axios.create({
  baseURL: config.axiosBaseUrl,
});

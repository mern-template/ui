const url = {
  users: {
    login: "auth/login",
    register: "auth/register",
    list: "users/list",
    profile: "users/profile",
    find: "users/find",
  },
  teams: {
    add: "teams/add",
    list: "teams/list",
    profile: "teams/profile",
  },
};

export default url;

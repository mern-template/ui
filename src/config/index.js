const config = {
  axiosBaseUrl: process.env.REACT_APP_URL,
  dashboardTabs: {
    admin: {
      tickets: {
        title: "تیکت ها",
        slug: "tickets",
        icon: "user",
      },
    },
  },
};

export default config;

import { ColorModeScript } from "@chakra-ui/react";
import React, { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";

//components
import App from "./app";
import theme from "src/styles/chakra-ui/theme";
import "src/styles/index.css";
import "react-image-crop/dist/ReactCrop.css";

//redux
import { Provider } from "react-redux";
import store from "src/redux/store";

const root = createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <StrictMode>
      <ColorModeScript />
      <ChakraProvider theme={theme}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ChakraProvider>
    </StrictMode>
  </Provider>
);

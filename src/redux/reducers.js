import { combineReducers } from "redux";
import appReducer from "src/redux/app/reducers";
import userReducer from "src/redux/user/reducers";

export default combineReducers({
  app: appReducer,
  user: userReducer,
});

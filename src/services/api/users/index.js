import { API, URL } from "src/config/api";

export const getUser = async (obj) => {
  try {
    const response = await API.post(URL.users.find, obj);
    return Promise.resolve(response.data);
  } catch (err) {
    return Promise.reject(err.response.data);
  }
};

import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  direction: "rtl",
  fonts: {
    heading: "irancell-bold",
    body: "irancell-regular",
  },
  colors: {
    white: "#ffffff",
    black: "#333",
    primary: {
      50: "#e1f3ff",
      100: "#bcd9f5",
      200: "#95bfea",
      300: "#6da5de",
      400: "#468bd3",
      500: "#2c72b9",
      600: "#1f5891",
      700: "#133f69",
      800: "#062642",
      900: "#000e1c",
    },
  },
  components: {
    Container: {
      baseStyle: {
        maxW: {
          sm: "30em",
          md: "95%",
          lg: "95%",
          xl: "95%",
          "2xl": "96em",
        },
      },
    },
    Text: {
      baseStyle: {},
      variants: {},
    },
    Heading: {
      defaultProps: {
        size: "md",
      },
    },
    Select: {
      variants: {
        primary: {
          field: {
            _placeholder: { color: "gray.400" },
            bg: "#F3F6FE",
            color: "black",
            height: "70px",
            textAlign: "center",
            borderRadius: "xl",
            _disabled: {
              opacity: 0.5,
            },
          },
        },
      },
    },
    Textarea: {
      variants: {
        primary: {},
      },
    },
    Input: {
      variants: {
        primary: {
          field: {
            _placeholder: { color: "primary.300" },
            bg: "primary.100",
            color: "black",
            height: "65px",
            textAlign: "center",
            borderRadius: "xl",
            _disabled: {
              opacity: 0.5,
            },
          },
        },
      },
    },
    Button: {
      defaultProps: {
        colorScheme: "primary",
        size: "xl",
      },
      baseStyle: {
        w: "100%",
        borderRadius: "xl",
        color: "white",
      },
      sizes: {
        xl: {
          h: "70px",
        },
      },
      variants: {
        primary: {},
      },
    },
  },
});

export default theme;

import React from "react";
import { Flex, Box, Spinner } from "@chakra-ui/react";
import {} from "./style";

const Loader = () => {
  return (
    <Flex
      flex={1}
      flexDirection="column"
      justify="center"
      align="center"
      w="100%"
      h="100vh"
      bg="primary.400"
    >
      <Spinner size="xl" color="#fff" />
    </Flex>
  );
};

export default Loader;

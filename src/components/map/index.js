import React, { useState } from "react";
import { MapWrapper, MapIcon } from "./style";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvents,
} from "react-leaflet";
import { mapIcon } from "./icon";
import { Link, VStack, Button } from "@chakra-ui/react";

const position = [35.755688, 51.445665];
const zoom = 18;

const Map = ({ en }) => {
  const [lat, setLat] = useState(position[0]);
  const [lng, setLng] = useState(position[1]);
  const GetCurrentLocation = () => {
    const map = useMapEvents({
      mouseup: (location) => {
        const {
          latlng: { lat, lng },
        } = location;
        setLat(lat);
        setLng(lng);
      },
    });
    return null;
  };

  return (
    <VStack w="100%">
      <MapWrapper>
        <MapContainer center={position} zoom={zoom}>
          <GetCurrentLocation />
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position} icon={mapIcon}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </MapContainer>
        <MapIcon icon="location-pin" size="48px" />
      </MapWrapper>
      <Link
        w="100%"
        isExternal
        href={
          "https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=35.755688,51.445665"
        }
      >
        <Button w="100%" h="80px">
          {!en ? "مسیریابی با نقشه" : "Navigation"}
        </Button>
      </Link>
    </VStack>
  );
};

export default Map;

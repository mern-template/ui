import styled from "styled-components";
import Icon from "src/config/icon.js";
import { Button } from "@chakra-ui/react";

export const MapWrapper = styled.div`
  width: 100%;
  height: 405px;
  position: relative;
  border-radius: 0.75rem;
  overflow: hidden;
  z-index: 1;
  margin-bottom: 7px;
`;

export const MapIcon = styled(Icon)`
  z-index: 999;
  color: #e50127;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

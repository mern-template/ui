import L from "leaflet";
import pin from "src/images/files/pin.svg";

const mapIcon = new L.Icon({
  iconUrl: pin,
  iconRetinaUrl: pin,
  iconAnchor: null,
  popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: new L.Point(48, 48),
  className: "leaflet-map-icon",
});

export { mapIcon };

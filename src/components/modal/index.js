import {
  Button,
  Modal as ModalContainer,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from "@chakra-ui/react";

const Modal = ({ children, isOpen, onClose, title, size }) => {
  return (
    <ModalContainer
      size={size || "4xl"}
      blockScrollOnMount={true}
      closeOnOverlayClick={true}
      onClose={onClose}
      isOpen={isOpen}
    >
      <ModalOverlay />
      <ModalContent pb="10px">
        <ModalHeader>{title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>{children}</ModalBody>
      </ModalContent>
    </ModalContainer>
  );
};

export default Modal;

import React, { useState, useRef, useEffect } from "react";
import ReactCrop from "react-image-crop";
import { Flex, Image, Button, Heading, VStack } from "@chakra-ui/react";
import {
  useDebounceEffect,
  centerAspectCrop,
  resizedCanvas,
  aspect,
  rotate,
  scale,
} from "./utils";
import { canvasPreview } from "./canvasPreview";

const ImageCropper = ({
  closeModal,
  image,
  uploadNewImage,
  showPreview = true,
}) => {
  const editMode = typeof image === "string";
  //Refs
  const previewCanvasRef = useRef(null);
  const imgRef = useRef(null);

  //State
  const [currentImage, setCurrentImage] = useState("");
  const [crop, setCrop] = useState();
  const [completedCrop, setCompletedCrop] = useState();

  useEffect(() => {
    if (editMode) {
      setCurrentImage({
        src: image,
        info: {},
      });
    } else {
      const info = image;
      setCrop(undefined);
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        setCurrentImage({
          src: reader.result.toString() || "",
          info,
        })
      );
      reader.readAsDataURL(image);
    }
  }, []);

  const onImageLoad = (e) => {
    if (aspect) {
      const { width, height } = e.currentTarget;
      setCrop(centerAspectCrop(width, height, aspect));
    }
  };

  useDebounceEffect(
    async () => {
      if (
        completedCrop?.width &&
        completedCrop?.height &&
        imgRef.current &&
        previewCanvasRef.current
      ) {
        canvasPreview(
          imgRef.current,
          previewCanvasRef.current,
          completedCrop,
          scale,
          rotate
        );
      }
    },
    100,
    [completedCrop, scale, rotate]
  );

  const onClickBtn = () => {
    if (editMode) {
      uploadNewImage();
    } else {
      const base64 = previewCanvasRef.current.toDataURL(currentImage.info.type);
      resizedCanvas(base64).then((response) => {
        closeModal(response);
      });
    }
  };

  return (
    <Flex flexDir={"column"}>
      <VStack justify="center" align={"center"} spacing={10}>
        {Boolean(currentImage) && (
          <Flex flexDir={"column"}>
            {!editMode ? (
              <ReactCrop
                crop={crop}
                onChange={(_, percentCrop) => setCrop(percentCrop)}
                onComplete={(c) => setCompletedCrop(c)}
                aspect={aspect}
                keepSelection={true}
                circularCrop={true}
                style={{
                  width: "100%",
                  maxWidth: 800,
                  display: "block",
                  borderRadius: "1rem",
                }}
              >
                <Image
                  ref={imgRef}
                  alt="Crop me"
                  src={currentImage.src}
                  style={{ transform: `scale(${scale}) rotate(${rotate}deg)` }}
                  onLoad={onImageLoad}
                  borderRadius="2xl"
                />
              </ReactCrop>
            ) : (
              <Image
                ref={imgRef}
                alt="Crop me"
                src={currentImage.src}
                style={{ transform: `scale(${scale}) rotate(${rotate}deg)` }}
                onLoad={onImageLoad}
                borderRadius="2xl"
              />
            )}
            <Button
              mt="20px"
              onClick={onClickBtn}
              isDisabled={!Boolean(currentImage)}
            >
              {editMode ? "حذف و آپلود عکس جدید" : "آپلود عکس پروفایل"}
            </Button>
          </Flex>
        )}
        {showPreview && Boolean(completedCrop) && (
          <Flex
            flexDir={"column"}
            justify="center"
            align={"center"}
            display="none"
          >
            <Heading mb="20px">پیش نمایش عکس شما</Heading>
            <canvas
              ref={previewCanvasRef}
              style={{
                borderRadius: "999px",
                width: completedCrop.width,
                height: completedCrop.height,
              }}
            />
          </Flex>
        )}
      </VStack>
    </Flex>
  );
};

export default ImageCropper;

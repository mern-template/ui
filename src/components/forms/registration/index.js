import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  Flex,
  Button,
  Box,
  Image,
  Link,
  FormLabel,
  FormControl,
} from "@chakra-ui/react";
import { IMAGES } from "src/images";
import forms from "src/forms/registration";
import list from "src/data";

//redux
import { useDispatch } from "react-redux";
import { loginUser, registerUser } from "src/redux/user/actions";

//components
import { useToastHook } from "src/components/toast";
import Input from "src/components/input";

const RegistrationForm = () => {
  const dispatch = useDispatch();
  const [toast, makeToast] = useToastHook();
  const [loginMode, setLoginMode] = useState(true);
  const form = loginMode ? forms["login"] : forms["register"];
  const {
    handleSubmit,
    register,
    getValues,
    control,
    reset,
    formState: { errors, isSubmitting },
  } = useForm();

  useEffect(() => {
    let defaultValues = {};
    Object.keys(form).map((key) => {
      if (key === "sex") {
        defaultValues[key] = "مرد";
      } else {
        defaultValues[key] = "";
      }
    });
    reset(defaultValues);
  }, [loginMode]);

  const onSubmit = (inputs) => {
    const dispatchAction = loginMode ? loginUser(inputs) : registerUser(inputs);
    if (!loginMode && inputs.password !== inputs.rePassword) {
      return makeToast({
        title: "کلمه عبور و تکرار آن باید با هم برابر باشند",
        status: "error",
      });
    } else {
      dispatch(dispatchAction)
        .then((res) => {
          if (loginMode) {
            makeToast({
              title: "با موفقیت وارد شدید",
              status: "success",
            });
          } else {
            makeToast({
              title: "در حال وارد شدن به پنل کاربری، لطفا صبر کنید...",
              status: "success",
            });
            dispatch(loginUser(inputs));
          }
        })
        .catch((err) => {
          let errorArray = [];
          if (Array.isArray(err)) {
            errorArray = err;
          } else {
            errorArray.push(err.message);
          }
          errorArray.map((error) => {
            setTimeout(() => {
              makeToast({
                title: error || "مشکلی در ارتباط با سرور وجود دارد",
                status: "error",
              });
            }, 50);
          });
        });
    }
  };

  const formIsInvalid = Object.keys(getValues()).some((key) => errors[key]);
  const link = loginMode ? "ثبت نام در پرابلم" : "ورود به حساب کاربری";
  const btnName = loginMode ? "ورود به پنل" : "ثبت نام";

  return (
    <Flex
      p={"40px"}
      pt="50px"
      bg="#00000080"
      borderRadius={"3xl"}
      boxShadow="2xl"
      flexDir={"column"}
      justify="center"
      align={"center"}
      w="100%"
      maxW="400px"
    >
      <Image
        src={IMAGES.logo}
        width={"150px"}
        display={"block"}
        m={"0 auto"}
        mb="10px"
      />
      <form onSubmit={handleSubmit(onSubmit)} className="custom-form">
        <FormControl isInvalid={formIsInvalid} mb="60px" mt="10px">
          {Object.entries(form).map((item) => {
            const fieldName = item[0];
            const input = item[1];
            let { label, placeholder, validations, type } = input;
            return (
              <Box key={`fieldName_${fieldName}`} mb="20px">
                <FormLabel htmlFor={fieldName} color="white">
                  {label}
                </FormLabel>
                <Input
                  config={{
                    fieldName,
                    placeholder,
                    type,
                    control,
                    data: list[fieldName] || [],
                    value: getValues(fieldName),
                    hookFormConfig: { ...register(fieldName, validations) },
                    disableOnUpdate: false,
                    error: errors[fieldName],
                  }}
                />
              </Box>
            );
          })}
        </FormControl>
        <Button mb={"20px"} type="submit" isLoading={isSubmitting}>
          {btnName}
        </Button>
      </form>
      <Link
        textDecoration={"underline"}
        color="primary.400"
        onClick={() => {
          setLoginMode(!loginMode);
        }}
      >
        {link}
      </Link>
    </Flex>
  );
};

export default RegistrationForm;

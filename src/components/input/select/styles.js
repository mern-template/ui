import styled from "styled-components";
import FuzzyReactSelect from "./FuzzyReactSelect";

export const SearchInputBox = styled(FuzzyReactSelect)`
  .css-1s2u09g-control {
    width: 100%;
    height: 65px;
    border-radius: 12px;
    border: none;
    background: rgb(188 217 245);
    cursor: pointer;
  }
  .css-1pahdxg-control,
  .css-1pahdxg-control:hover {
    height: 65px;
    border-radius: 12px;
    border: none;
    background: rgb(188 217 245);
  }
  .css-tlfecz-indicatorContainer {
    color: hsl(209deg 67% 66%);
  }
  .css-1okebmr-indicatorSeparator {
    background-color: hsl(209deg 67% 66%);
  }
  .css-14el2xx-placeholder {
    color: #333;
  }
  //class-popUp-searchInputBox
  .css-26l3qy-menu {
    z-index: 3;
    * {
      cursor: pointer;
    }
  }
  * {
    text-align: center;
    direction: rtl;
  }
`;

import React from "react";
import { SearchInputBox } from "./styles";

const fuzzyBaseOptions = {
  maxPatternLength: 8,
  includeScore: true,
  maxResults: 25,
  threshold: 0.3,
};

const SelectInput = ({
  data,
  onChange,
  propertyName,
  isDisabled,
  config: { type, field, placeholder },
  ...props
}) => {
  const fuzzyOptions = {
    keys: [{ name: propertyName, weight: 1 }],
    valueKey: propertyName,
    ...fuzzyBaseOptions,
  };
  return (
    <SearchInputBox
      {...props}
      isDisabled={isDisabled || false}
      options={data}
      fuzzyOptions={fuzzyOptions}
      autoCorrect="off"
      spellCheck="off"
      placeholder={placeholder}
      onChange={(item) => onChange(type, field, item.value)}
      className="custom-search-box"
    />
  );
};

export default SelectInput;

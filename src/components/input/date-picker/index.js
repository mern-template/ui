import React from "react";
import { Controller } from "react-hook-form";

import DatePicker from "react-multi-date-picker";
import persian from "react-date-object/calendars/persian";
import persian_fa from "react-date-object/locales/persian_fa";

const DatePickerComponent = (props) => {
  const {
    config: { control, fieldName, placeholder, value },
    onChange,
  } = props;
  return (
    <Controller
      control={control}
      name={fieldName}
      rules={{ required: true }}
      render={({ field: { ...rest } }) => {
        return <p>asds</p>;
      }}
    />
  );
};

export default DatePickerComponent;

import React from "react";
import { Input } from "@chakra-ui/react";
import { Controller } from "react-hook-form";

//datePicker
import DatePicker from "react-multi-date-picker";
import persian from "react-date-object/calendars/persian";
import persian_fa from "react-date-object/locales/persian_fa";

//selectInput
import { ReactSelectComponent as ReactSelect } from "./style";

const InputComponent = (props) => {
  let renderInputsByTypes;
  let {
    config: {
      fieldName,
      placeholder,
      type,
      control,
      data,
      value,
      disableOnUpdate,
      hookFormConfig,
      error,
    },
  } = props;

  switch (type) {
    case "select":
      placeholder = value || placeholder;
      renderInputsByTypes = (
        <Controller
          name={fieldName}
          control={control}
          render={({ field: { onChange, value, ...fieldProps } }) => (
            <ReactSelect
              options={data}
              placeholder={placeholder}
              onChange={(item) => onChange(item.value)}
              className={error && "error-input"}
              {...fieldProps}
            />
          )}
        />
      );
      break;
    case "date":
      renderInputsByTypes = (
        <Controller
          name={fieldName}
          control={control}
          render={({ field: { onChange, value, ...fieldProps } }) => (
            <DatePicker
              containerStyle={{
                width: "100%",
                height: "65px",
              }}
              style={{
                width: "100%",
                height: "65px",
                borderRadius: "12px",
                border: "none",
                background: "rgb(188 217 245)",
                cursor: "pointer",
                textAlign: "center",
                border: error && "2px solid red",
                background: "rgb(188 217 245)",
              }}
              placeholder={placeholder}
              calendar={persian}
              locale={persian_fa}
              onOpenPickNewDate={false}
              value={value}
              onChange={(date) => {
                const {
                  year,
                  month: { number: monthNumber },
                  day,
                } = date;
                const selectedDate = "".concat(
                  year,
                  "/",
                  monthNumber,
                  "/",
                  day
                );
                onChange(selectedDate);
              }}
              format={"YYYY/MM/DD"}
              {...fieldProps}
            />
          )}
        />
      );
      break;
    default:
      renderInputsByTypes = (
        <Input
          id={fieldName}
          placeholder={placeholder}
          type={type}
          control={control}
          variant={"primary"}
          disabled={disableOnUpdate}
          {...props}
          {...hookFormConfig}
          className={error && "error-input"}
        />
      );
      break;
  }
  return renderInputsByTypes;
};

export default InputComponent;

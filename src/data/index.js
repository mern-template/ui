import province from "./provinces";
import education from "./education";
import sex from "./sex";

const list = {
  province,
  education,
  sex,
};

export default list;

const data = [
  {
    label: "دیپلم",
    value: "دیپلم",
  },
  {
    label: "کاردانی",
    value: "کاردانی",
  },
  {
    label: "کارشناسی",
    value: "کارشناسی",
  },
  {
    label: "کارشناسی ارشد",
    value: "کارشناسی ارشد",
  },
  {
    label: "دکترا",
    value: "دکترا",
  },
];

export default data;
